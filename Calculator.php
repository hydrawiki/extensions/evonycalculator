<?php

if (function_exists('wfLoadExtension')) {
	wfLoadExtension('EvonyCalculator');
	wfWarn(
		'Deprecated PHP entry point used for EvonyCalculator extension. Please use wfLoadExtension instead, ' .
		'see https://www.mediawiki.org/wiki/Extension_registration for more details.'
	);
	return;
} else {
	die( 'This version of the EvonyCalculator extension requires MediaWiki 1.25+' );
}
