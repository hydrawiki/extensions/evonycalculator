var currTab = 'Town';

function getByClass(classname, tagname) {
    if (document.getElementsByClassName) {
        return document.getElementsByClassName(classname);
    }
    else{
        var tempArray = document.getElementsByTagName(tagname);
        var retArray = new Array;
        for (var bob in tempArray) {
            if (tempArray[bob].className == classname)
                retArray.push(tempArray[bob]);
        }
        return retArray;
    }
}

window.onload = function() {
    //Change tabs function assigning
    var tempArray = document.getElementById('tabsblock').childNodes; //Get calc tabs
    for (var i in tempArray)
        tempArray[i].onclick = changeTab;
    
    //Change level function assigning
    tempArray = getByClass('levelInput', 'input');
    for (var i in tempArray)
        tempArray[i].onchange = changeLevel;
    
    //Change amount function assigning
    tempArray = getByClass('amountInput', 'input');
    for (var i in tempArray)
        tempArray[i].onchange = changeAmount;
    
    //Change hero points function assigning
    tempArray = getByClass('heroInput', 'input');
    for (var i in tempArray)
        tempArray[i].onchange = changeHeroPoints;
    
    //Change object function assigning
    tempArray = getByClass('obSelector', 'select');
    for (var i in tempArray) {
        tempArray[i].onchange = changeObject;
        tempArray[i].onkeyup = changeObject;
    }
    
    //Change mayor
    tempArray = getByClass('mayorCheck', 'select');
    for (var i in tempArray) {
        tempArray[i].onchange = changeMayor;
    }
    
    //Toggle links button
    document.getElementById('toggleLinks').onclick = toggleLinks;
    document.getElementById('resetAll').onclick = resetAll;
    document.getElementById('updateLinks').onclick = updateLink;
    
    document.getElementById('selurl').onclick = function() {document.getElementById('linkurl').focus(); document.getElementById('linkurl').select();};
    document.getElementById('selbb').onclick = function() {document.getElementById('linkbbcode').focus(); document.getElementById('linkbbcode').select();};
    document.getElementById('selhtml').onclick = function() {document.getElementById('linkhtml').focus(); document.getElementById('linkhtml').select();};
    
    //if (typeof(importedBuild) != 'undefined')
    //    loadLink(importedBuild);
    //else {
    //    updateAvail(1);
    //}
    //updateAllObjects();
    
     //alert(window.location.hash);
    
    if(window.location.hash != '')
          loadLink(window.location.hash.substring(7));
    //if (typeof(importedBuild) != 'undefined')
    //    loadLink(importedBuild);
    else {
        updateAvail(1);
    }
    updateAllObjects();

    
    document.getElementById('loading').style.display = 'none';
    //document.getElementById('tab' + currTab).style.backgroundColor = 'rgb(245,175,30)';
    document.getElementById('bloc' + currTab).style.display = 'block';
};

function toggleLinks() {
    if (document.getElementById('linkBlock').style.display == 'block') {
        document.getElementById('linkBlock').style.display = 'none';
        this.innerHTML = 'Show links';
        this.style.color = 'green';
    }
    else {
        document.getElementById('linkBlock').style.display = 'block';
        this.innerHTML = 'Hide links';
        this.style.color = 'red';
        updateLink();
    }
}

function resetAll() {
    //Change level function assigning
    tempArray = getByClass('levelInput', 'input');
    for (var i in tempArray)
        tempArray[i].value = 0;
    
    //Set town hall to 1
    document.getElementById('town1_level').value = 1;
    
    //Change amount function assigning
    tempArray = getByClass('amountInput', 'input');
    for (var i in tempArray)
        tempArray[i].value = 0;
        
    //Change hero points function assigning
    tempArray = getByClass('heroInput', 'input');
    for (var i in tempArray)
        tempArray[i].value = 0;
    
    tempArray = document.getElementsByTagName('select');
    for (var i in tempArray)
        tempArray[i].selectedIndex = 0;
    
    updateAllObjects();
    updateAvail(1);
}

function changeTab() {
    var newTab = this.id.replace('tab', '');
    
    if (newTab != currTab) {
        document.getElementById('bloc' + currTab).style.display = 'none';
        document.getElementById('bloc' + newTab).style.display = 'block';
        
        this.className = 'active';
        document.getElementById('tab' + currTab).className = '';
        
        currTab = newTab;
    }
}

function changeObject() {
    /*if (document.getElementById(this.id + '_img')) {
        if (this.value != 'null') {
             document.getElementById(this.id + '_img').src = '/extensions/EvonyCalculator/images/' + this.value + '.jpg';
            document.getElementById(this.id + '_img').style.display = 'inline';
        }
        else {
            document.getElementById(this.id + '_img').style.display = 'none';
        }
    }*/
    
    //Change stats
    updateObject(this.id);
    updateResources();
}

function changeLevel() { // Check level input
    if (IsNumeric(this.value)) {
        if (parseInt(this.value) > 10)
            this.value = 10;
        else if (parseInt(this.value) < 0)
            this.value = 0;
    }
    else
        this.value = 0;
    
        
    if (this.id == 'town1_level') {
        if (parseInt(this.value) < 1)
            this.value = 1;
        updateAvail(this.value);
    }
    
    updateObject(this.id.replace('_level', ''));
    updateResources();
    
    if (this.id == 'researches_construction_level')
        updateAllObjects('buildings');
    else if (this.id == 'researches_militaryScience_level')
        updateAllObjects('troops');
    else if (this.id == 'researches_metalCasting_level')
        updateAllObjects('mech');
}

function changeHeroPoints() { // Check level input
    if (IsNumeric(this.value)) {
        if (parseInt(this.value) > 500)
            this.value = 500;
        else if (parseInt(this.value) < 0)
            this.value = 0;
    }
    else
        this.value = 0;
    
    var daHero = this.id.split('_');
    if (document.getElementById(daHero[0] + '_mayor').checked) {
        if (daHero[1] == 'politics')
            updateResources();
        else if (daHero[1] == 'attack')
            updateAllObjects('units');
        else if (daHero[1] == 'intelligence')
            updateAllObjects('researches');
    }
}

function changeMayor() {
    /*for (var i = 1; i <= 10; i++) {
        if (this.id != 'hero' + i + '_mayor')
            document.getElementById('hero' + i + '_mayor').checked = false;
    }*/
    
    updateResources();
}

function changeAmount() { // Check amount input
    if (IsNumeric(this.value)) {
        if (parseInt(this.value) > 1000000)
            this.value = 1000000;
        else if (parseInt(this.value) < 0)
            this.value = 0;
    }
    else
        this.value = 0;
    
    updateObject(this.id.replace('_level', ''));
    updateResources();
}

function updateAvail(lvl) {
    var avail = 13 + ((lvl - 1) * 3);
    
    for (var i = 1; i <= 40; i++) {
        if (i <= avail) {
            document.getElementById('city' + i + '_row').style.visibility = 'visible';
        }
        else {
            document.getElementById('city' + i + '_level').value = 0;
            document.getElementById('city' + i + '_row').style.visibility = 'hidden';
        }
    }
    
    for (i = 1; i <= 10; i++) {
        if (i <= lvl) {
            document.getElementById('valley' + i + '_row').style.visibility = 'visible';
        }
        else {
            document.getElementById('valley' + i + '_level').value = 0;
            document.getElementById('valley' + i + '_row').style.visibility = 'hidden';
        }
    }
}

function updateValley(id) {
    var ob = getObject(id);
    var lvl = getLevel(id);
    
    if (ob == 'null') {
        document.getElementById(id + '_type').innerHTML = '';
        document.getElementById(id + '_amount').innerHTML = '';
    }
    else {
        document.getElementById(id + '_type').innerHTML = objects['Valleys'][ob].type;
        document.getElementById(id + '_amount').innerHTML = lvl * objects['Valleys'][ob].up + objects['Valleys'][ob].start + '%';
    }
    updateResources();
}

function updateObject (id, datab) {
    if (typeof(datab) == 'undefined')
        datab = currTab;
    
    if (currTab == 'Valleys') {
        updateValley(id);
        return;
    }
    
    var ob = getObject(id);
    var lvl = getLevel(id);
    
    if (document.getElementById(id + '_img')) {
        if (this.value != 'null') {
            document.getElementById(id + '_img').src = '/extensions/EvonyCalculator/images/' + ob + '.jpg';
            document.getElementById(id + '_img').style.display = 'inline';
        }
        else {
            document.getElementById(id + '_img').style.display = 'none';
        }
    }
    
    if (ob == 'null') {
        for (var a in objects[datab]) {
            for (var b in objects[datab][a])
                document.getElementById(id + '_' + b).innerHTML = '';
            break;
        }
    }
    else {
        if (lvl < 10 || (datab == 'Troops') || (datab == 'Walls')) {
            var tempNum = 0;
            var tMayor = getMayor();
            if (typeof(tMayor) == 'undefined')
                tMayor = '1';
            
            for (var b in objects[datab][ob]) {
                if (datab == 'Troops' || datab == 'Walls')
                    tempNum = (lvl * objects[datab][ob][b]);
                else
                    tempNum = (Math.pow(2, lvl) * objects[datab][ob][b]);
                
                if (b == 'time') {
                    //Amount reduced by research
                    if (datab == 'Researches')
                        tempNum = Math.floor(tempNum * Math.pow(0.995, document.getElementById('hero' + tMayor + '_intelligence').value));
                    else if (datab == 'Troops') {
                        if (ob == 'transporter' || ob == 'ballista' || ob == 'batteringRam' || ob == 'catapult')
                            tempNum = Math.floor((tempNum * (1 - getLevel('researches_metalCasting') * 0.05)) * Math.pow(0.995, document.getElementById('hero' + tMayor + '_attack').value));
                        else
                            tempNum = Math.floor((tempNum * (1 - getLevel('researches_militaryScience') * 0.05)) * Math.pow(0.995, document.getElementById('hero' + tMayor + '_attack').value));
                    }
                    else
                        tempNum = Math.floor((tempNum * Math.pow(0.995, document.getElementById('hero' + tMayor + '_politics').value)) * Math.pow(0.9, getLevel('researches_construction')));
                        //tempNum = Math.floor(tempNum * (100 - (document.getElementById('researches_construction_level').value * 5)) / 100);
                    
                    if (tempNum >= 3600) {
                        var seconds = tempNum % 60;
                        if (seconds < 10)
                        seconds = '0' + seconds;
                        
                        var minutes = Math.floor((tempNum % 3600) / 60);
                        if (minutes < 10)
                        minutes = '0' + minutes;
                        
                        document.getElementById(id + '_' + b).innerHTML = Math.floor(tempNum / 3600) + ':' + minutes + ':' + seconds;
                    }
                    else if (tempNum >= 60) {
                        var seconds = tempNum % 60;
                        if (seconds < 10)
                        seconds = '0' + seconds;
                        
                        document.getElementById(id + '_' + b).innerHTML = Math.floor(tempNum / 60) + ':' + seconds;
                    }
                    else {
                        document.getElementById(id + '_' + b).innerHTML = tempNum;
                    }
                }
                else {
                    if (tempNum > 1000000000)
                        document.getElementById(id + '_' + b).innerHTML = tempNum / 1000000000 + 'B';
                    else if (tempNum > 1000000)
                        document.getElementById(id + '_' + b).innerHTML = tempNum / 1000000 + 'M';
                    else if (tempNum > 1000)
                        document.getElementById(id + '_' + b).innerHTML = tempNum / 1000 + 'K';
                    else
                        document.getElementById(id + '_' + b).innerHTML = tempNum;
                }
            }
        }
        else {
            for (var b in objects[datab][ob])
                document.getElementById(id + '_' + b).innerHTML = '';
        }
    }
    updateResources();
}

function updateAllObjects(type) {
    if (typeof(type) == 'undefined')
        type = 'all';
    var hallLvl = document.getElementById('town1_level').value;
    
    if (type == 'all' || type == 'buildings') {
        //Town
        updateObject('town1', 'Town'); //Town hall
        updateObject('town2', 'Town'); //Walls
        
        for (var i = 3; i <= 34; i++) {
            //if (document.getElementById('town' + i).value != 'null')
                updateObject ('town' + i, 'Town');
        }
        
        //City
        for ( i = 1; i <= (13 + (hallLvl - 1) * 3); i++) {
            //if (document.getElementById('city' + i).value != 'null')
            updateObject('city' + i, 'City');
        }
        
        //Valleys
        for (i = 1; i <= hallLvl; i++) {
            updateValley('valley' + i);
        }
    }
    
    if (type == 'all' || type == 'units' || type == 'troops') {
        //Walls
        for (var j in objects['Walls']) {
            updateObject ('walls_' + j, 'Walls');
        }
    }
    
    if (type == 'all' || type == 'units' || type == 'mech') {
        //Troops
        for (var j in objects['Troops']) {
            updateObject ('troops_' + j, 'Troops');
        }
    }
    
    if (type == 'all' || type == 'researches') {
        //Researches
        for (var j in objects['Researches']) {
            updateObject ('researches_' + j, 'Researches');
        }
    }
}

function updateResources() {
    /* ----- Basic production ----- */
    //food
    var valFood = 0;
    for (var i = 1; i <= 34; i++) {
        if (document.getElementById('city' + i).value == 'farm') {
            var clvl = getLevel('city' + i);
            valFood += Math.floor((clvl * (clvl + 1) / 2) * 100);
        }
    }
    document.getElementById('production_food').innerHTML = valFood;
    
    //lumber
    var valLumber = 0;
    for (var i = 1; i <= 34; i++) {
        var clvl = getLevel('city' + i);
        if (document.getElementById('city' + i).value == 'sawmill') {
            valLumber += Math.floor((clvl * (clvl + 1) / 2) * 100);
        }
    }
    document.getElementById('production_lumber').innerHTML = valLumber;
    
    //stone
    var valStone = 0;
    for (var i = 1; i <= 34; i++) {
        var clvl = getLevel('city' + i);
        if (document.getElementById('city' + i).value == 'quarry') {
            valStone += Math.floor((clvl * (clvl + 1) / 2) * 100);
        }
    }
    document.getElementById('production_stone').innerHTML = valStone;
    
    //iron
    var valIron = 0;
    for (var i = 1; i <= 34; i++) {
        var clvl = getLevel('city' + i);
        if (document.getElementById('city' + i).value == 'ironmine') {
            valIron += Math.floor((clvl * (clvl + 1) / 2) * 100);
        }
    }
    document.getElementById('production_iron').innerHTML = valIron;
    
    /* ----- Providence ----- */
    var providence = 0;
    if (document.getElementById('town1_level').value > 0)
        providence = 100;
    
    document.getElementById('providence_food').innerHTML = providence;
    document.getElementById('providence_lumber').innerHTML = providence;
    document.getElementById('providence_stone').innerHTML = providence;
    document.getElementById('providence_iron').innerHTML = providence;
    
    /* ----- Technologies ----- */
    var techFood = Math.floor((document.getElementById('researches_agriculture_level').value * 0.1) * valFood);
    var techLumber = Math.floor((document.getElementById('researches_lumbering_level').value * 0.1) * valLumber);
    var techStone = Math.floor((document.getElementById('researches_masonry_level').value * 0.1) * valStone);
    var techIron = Math.floor((document.getElementById('researches_metalCasting_level').value * 0.1) * valIron);
    document.getElementById('tech_food').innerHTML = techFood;
    document.getElementById('tech_lumber').innerHTML = techLumber;
    document.getElementById('tech_stone').innerHTML = techStone;
    document.getElementById('tech_iron').innerHTML = techIron;
    
    /* ----- Troops upkeep ----- */
    var foodUpkeep = 0;
    for (i in objects['Troops']) {
        foodUpkeep += getLevel('troops_' + i) * upkeep[i];
    }
    document.getElementById('troops_food').innerHTML = foodUpkeep;
    
    /* ----- Hero plus ----- */
    var politics = 0;
    var mayor = getMayor();
    
    politics = document.getElementById('hero' + mayor + '_politics').value;
    
    polFood = Math.floor((politics * 0.01) * valFood);
    polLumber = Math.floor((politics * 0.01) * valLumber);
    polStone = Math.floor((politics * 0.01) * valStone);
    polIron = Math.floor((politics * 0.01) * valIron);
    
    document.getElementById('hero_food').innerHTML = polFood;
    document.getElementById('hero_lumber').innerHTML = polLumber;
    document.getElementById('hero_stone').innerHTML = polStone;
    document.getElementById('hero_iron').innerHTML = polIron;
    
    /* ----- Valleys plus ----- */
    var valleyFood = 0;
    var valleyLumber = 0;
    var valleyStone = 0;
    var valleyIron = 0;
    for (i = 1; i <= 10; i++) {
        if (document.getElementById('valley' + i).value == 'grassland' || document.getElementById('valley' + i).value == 'swamp')
            valleyFood += Math.floor((document.getElementById('valley' + i + '_level').value * 2 + 3) * 0.01 * valFood);
        else if (document.getElementById('valley' + i).value == 'lake')
            valleyFood += Math.floor((document.getElementById('valley' + i + '_level').value * 3 + 5) * 0.01 * valFood);
        else if (document.getElementById('valley' + i).value == 'forest')
            valleyLumber += Math.floor((document.getElementById('valley' + i + '_level').value * 2 + 3) * 0.01 * valLumber);
        else if (document.getElementById('valley' + i).value == 'desert')
            valleyStone += Math.floor((document.getElementById('valley' + i + '_level').value * 2 + 3) * 0.01 * valStone);
        else if (document.getElementById('valley' + i).value == 'hill')
            valleyIron += Math.floor((document.getElementById('valley' + i + '_level').value * 2 + 3) * 0.01 * valIron);
    }
    document.getElementById('valley_food').innerHTML = valleyFood;
    document.getElementById('valley_lumber').innerHTML = valleyLumber;
    document.getElementById('valley_stone').innerHTML = valleyStone;
    document.getElementById('valley_iron').innerHTML = valleyIron;
    
    /* ----- Total production ----- */
    document.getElementById('total_food').innerHTML = (valFood + providence + techFood + foodUpkeep + valleyFood + + polFood);
    document.getElementById('total_lumber').innerHTML = valLumber + providence + techLumber + valleyLumber + polLumber;
    document.getElementById('total_stone').innerHTML = valStone + providence + techStone + valleyStone + polStone;
    document.getElementById('total_iron').innerHTML = valIron + providence + techIron + valleyIron + polIron;
    
}

function updateLink() {
    //var link = 'http://localhost/evony_calc/index.php?build=';
    //var link = 'http://diabloexile.com/evony/index.php?build=';
    var link = 'http://evony.gamepedia.com/Calculator#build=';

    //Town
    if (getLevel('town1') == 10)//Town hall
        link += 'a';
    else
        link += getLevel('town1');
    if (getLevel('town2') == 10)//Walls
        link += 'a';
    else
        link += getLevel('town2');
    
    for (var i = 3; i <= 34; i++) {
        link += toSingleChar(document.getElementById('town' + i).selectedIndex);
        link += toSingleChar(document.getElementById('town' + i + '_level').value);
    }
    
    //City
    for (var i = 1; i <= 40; i++) {
        link += toSingleChar(document.getElementById('city' + i).selectedIndex);
        link += toSingleChar(document.getElementById('city' + i + '_level').value);
    }
    
    //Valleys
    for (var i = 1; i <= 10; i++) {
        link += toSingleChar(document.getElementById('valley' + i).selectedIndex);
        link += toSingleChar(document.getElementById('valley' + i + '_level').value);
    }
    
    //Researches
    for (var j in objects['Researches']) {
        link += toSingleChar(document.getElementById('researches_' + j + '_level').value);
    }
    
    //Walls
    for (var j in objects['Walls']) {
        link += 'l' + document.getElementById('walls_' + j + '_level').value;
    }
    
    //Troops
    for (var j in objects['Troops']) {
        link += 'l' + document.getElementById('troops_' + j + '_level').value;
    }
    
    //Heros
    for (var i = 1; i <= 10; i++) {
        link += 'l' + document.getElementById('hero' + i + '_level').value;
        link += 'l' + document.getElementById('hero' + i + '_politics').value;
        link += 'l' + document.getElementById('hero' + i + '_attack').value;
        link += 'l' + document.getElementById('hero' + i + '_intelligence').value;
    }
    
    document.getElementById('linkurl').value = link;
	document.getElementById('linkbbcode').value = '[url=' + link + ']My Evony calculation[/url]';
	document.getElementById('linkhtml').value = '<a href="' + link + '">My Evony calculation</a>';
}

function loadLink(build) {
    build = escape(build);
    var points = build.split('l');
    var pos = 2;
    
    //Town
    document.getElementById('town1_level').value = fromSingleChar(points[0].charAt(0));
    updateAvail(fromSingleChar(points[0].charAt(0)));
    document.getElementById('town2_level').value = fromSingleChar(points[0].charAt(1));
    for (var i = 3; i <= 34; i++) {
        document.getElementById('town' + i).selectedIndex = fromSingleChar(points[0].charAt(pos));
        pos++;
        document.getElementById('town' + i + '_level').value = fromSingleChar(points[0].charAt(pos));
        pos++;
    }
    
    //City
    for (var i = 1; i <= 40; i++) {
        document.getElementById('city' + i).selectedIndex = fromSingleChar(points[0].charAt(pos));
        pos++;
        document.getElementById('city' + i + '_level').value = fromSingleChar(points[0].charAt(pos));
        pos++;
    }
    
    //Valleys
    for (var i = 1; i <= 10; i++) {
        document.getElementById('valley' + i).selectedIndex = fromSingleChar(points[0].charAt(pos));
        pos++;
        document.getElementById('valley' + i + '_level').value = fromSingleChar(points[0].charAt(pos));
        pos++;
    }
    
    //Researches
    for (var j in objects['Researches']) {
        document.getElementById('researches_' + j + '_level').value = fromSingleChar(points[0].charAt(pos));
        pos ++;
    }
    
    pos = 1;
    
    //Walls
    for (var j in objects['Walls']) {
        document.getElementById('walls_' + j + '_level').value = parseInt(points[pos]);
        pos ++;
    }
    
    //Troops
    for (var j in objects['Troops']) {
        document.getElementById('troops_' + j + '_level').value = parseInt(points[pos]);
        pos ++;
    }
    
    //Heros
    for (var i = 1; i <= 10; i++) {
        document.getElementById('hero' + i + '_level').value = parseInt(points[pos]);
        pos ++;
        document.getElementById('hero' + i + '_politics').value = parseInt(points[pos]);
        pos ++;
        document.getElementById('hero' + i + '_attack').value = parseInt(points[pos]);
        pos ++;
        document.getElementById('hero' + i + '_intelligence').value = parseInt(points[pos]);
        pos ++;
    }
    
    document.getElementById('linkurl').value = build;
	document.getElementById('linkbbcode').value = '[url=' + build + ']My Evony calculation[/url]';
	document.getElementById('linkhtml').value = '<a href="' + build + '">My Evony calculation</a>';
}

function toSingleChar(num) {
    if (num < 10)
        return num;
    else if (num == 10)
        return 'a';
    else if (num == 11)
        return 'b';
    else if (num == 12)
        return 'c';
    else if (num == 13)
        return 'd';
    else if (num == 14)
        return 'e';
    else if (num == 15)
        return 'f';
    else if (num == 16)
        return 'g';
    else if (num == 17)
        return 'h';
    else if (num == 18)
        return 'i';
    else if (num == 19)
        return 'j';
    else if (num == 20)
        return 'k';
}

function fromSingleChar(num) {
    if (parseInt(num) < 10)
        return parseInt(num);
    else if (num == 'a')
        return 10;
    else if (num == 'b')
        return 11;
    else if (num == 'c')
        return 12;
    else if (num == 'd')
        return 13;
    else if (num == 'e')
        return 14;
    else if (num == 'f')
        return 15;
    else if (num == 'g')
        return 16;
    else if (num == 'h')
        return 17;
    else if (num == 'i')
        return 18;
    else if (num == 'j')
        return 19;
    else if (num == 'k')
        return 20;
}

function getObject(id) {//Get object database name
    if (id == 'town1')
        return 'townHall';
    else if (id == 'town2')
        return 'wall';
        
    var splited = id.split('_');
    
    if (splited[1])
        return splited[1];
    else
        return document.getElementById(id).value;
}

function getLevel(id) {//Get object level
    return parseInt(document.getElementById(id + '_level').value);
}

function getMayor() {
    for (var i = 1; i <= 10; i++)
        if (document.getElementById('hero' + i + '_mayor').checked)
            return i;
}

//www.codetoad.com
function IsNumeric(sText) {
   var ValidChars = "0123456789";
   var IsNumber=true;
   var Char;


   for (i = 0; i < sText.length && IsNumber == true; i++)
      {
      Char = sText.charAt(i);
      if (ValidChars.indexOf(Char) == -1)
         {
         IsNumber = false;
         }
      }
   return IsNumber;

}