<?php

class EvonyCalculatorHooks {
	static private $markerList = [];

	/**
	 * Registers our function hooks for displaying blocks of user points
	 *
	 * @access	public
	 * @param	object	Parser reference
	 * @return	boolean	true
	 */
	static public function onParserFirstCallInit(&$parser) {
		$parser->setHook('evCalculator', [__CLASS__, 'createCalculator']);
		return true;
	}

	/**
	 * Create the calculator.
	 *
	 * @access	public
	 * @param	string	Raw User Input
	 * @param	array	Arguments on the tag.
	 * @param	object	Parser object.
	 * @param	object	PPFrame object.
	 * @return	string	Error Message
	 */
	static public function createCalculator($input, array $args, Parser $parser, PPFrame $frame) {
		if ($parser instanceof Parser) {
			// dont call this if parser is null (such as in API usage).
			$out = $parser->getOutput();
			$out->addModules('ext.evonyCalculator.scripts');
			$out->addModuleStyles('ext.evonyCalculator.styles');
		}
		ob_start();
		?>
			<!-- ***** CALCULATOR ***** -->
				<div id="calcContent">
					<noscript>
						<h3>You MUST unable javascript in order to use the calculator.</h3>
					</noscript>
					<div>
						<span id="toggleLinks" class="clickable">Show links</span> - <span id="resetAll" class="clickable">Reset</span>
						<div id="linkBlock">
							<span id="updateLinks" class="clickable">Update links</span><br />
							<span class="mleft">URL:</span><input type="text" id="linkurl" size="50" /> <span id="selurl" class="clickable">[Select]</span><br />
							<span class="mleft">BBCode:</span><input type="text" id="linkbbcode" size="50" /> <span id="selbb" class="clickable">[Select]</span><br />
							<span class="mleft">HTML:</span><input type="text" id="linkhtml" size="50" /> <span id="selhtml" class="clickable">[Select]</span>
						</div>
					</div>
					<ul id="tabsblock" class="primary">
						<li id="tabTown" class="active"><a>Town</a></li>
						<li id="tabCity"><a>City</a></li>
						<li id="tabResearches"><a>Researches</a></li>
						<li id="tabResources"><a>Resources</a></li>
						<li id="tabHeros"><a>Heros</a></li>
						<li id="tabTroops"><a>Troops</a></li>
						<li id="tabWalls"><a>Walls</a></li>
						<li id="tabValleys"><a>Valleys</a></li>
					</ul>
					<div id="loading">
						<h3>Loading...</h3>
						<img src="/extensions/EvonyCalculator/images/loading.gif" alt="Loading" />
					</div>
					<?php
					include __DIR__.'/includes/town.html';
					include __DIR__.'/includes/city.html';
					include __DIR__.'/includes/researches.html';
					include __DIR__.'/includes/resources.html';
					include __DIR__.'/includes/heros.html';
					include __DIR__.'/includes/troops.html';
					include __DIR__.'/includes/walls.html';
					include __DIR__.'/includes/valleys.html';
					?>
				</div>
				<!-- ***** /CALCULATOR ***** -->
		<?php
		$output = ob_get_contents();

		ob_end_clean();

		$markercount = count(self::$markerList);
		$marker = "xx-marker".$markercount."-xx";
		self::$markerList[$markercount] = $output;
		return $marker;
	}

	static public function onParserAfterTidy($parser, &$text) {
		// find markers in $text
		// replace markers with actual output
		$keys = [];
		$marker_count = count(self::$markerList);

		for ($i = 0; $i < $marker_count; $i++) {
			$keys[] = 'xx-marker' . $i . '-xx';
		}

		$text = str_replace($keys, self::$markerList, $text);
		return true;
	}
}